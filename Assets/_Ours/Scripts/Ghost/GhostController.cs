using UnityEngine;

namespace Vivraan.KU.AiRport.Ghost
{
    public class GhostController : MonoBehaviour
    {
        [SerializeField] private float acceleration = 20F;
        [SerializeField] private float maxSpeed = 100F;
        [SerializeField] private float yawRate = 100F;

        private Vector3 velocity;
        private Vector3 input;

        // Update is called once per frame
        private void Update()
        {
            var thisTransform = transform;
            
            // Get input from the user
            input = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("UpDown"), Input.GetAxis("Vertical"));

            // Transform it
            input = thisTransform.TransformDirection(input);

            // Calculate the new velocity based on acceleration and input
            velocity = input.sqrMagnitude > 0F
                ? velocity + input * (acceleration * Time.deltaTime)
                : Vector3.zero;

            // Clamp the velocity to the maximum speed
            velocity = Vector3.ClampMagnitude(velocity, maxSpeed);

            // Apply the new position to the spectator object
            thisTransform.localPosition += velocity * Time.deltaTime;

            // Rotate the spectator object based on the input
            var newRotation = thisTransform.rotation;
            var yawPlus = OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger);
            var yawMinus = -OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger);
            newRotation *= Quaternion.AngleAxis((yawPlus + yawMinus) * yawRate * Time.deltaTime, Vector3.up);
            transform.rotation = newRotation;
        }
    }
}