using System.Collections.Generic;
using UnityEngine;

namespace Vivraan.KU.AiRport.Faked
{
    public class AircraftVizard : MonoBehaviour
    {
        [SerializeField]
        private GameObject aircraftPrefab;

        private ClientWrapper client;
        private Dictionary<int, GameObject> aircraftObjects;

        // Start is called before the first frame update
        private void Start()
        {
            client = FindObjectOfType<ClientWrapper>();
            aircraftObjects ??= new Dictionary<int, GameObject>();
            client.OnFlightsUpdated += UpdateAircraft;
            return;

            void UpdateAircraft(FakeFlightResponse response)
            {
                foreach (var newState in response.States)
                {
                    if (aircraftObjects.TryGetValue(newState.AircraftID, out var aircraftObject))
                    {
                        aircraftObject.transform.localPosition =
                            new Vector3(newState.Px, newState.Py, newState.Pz);
                        aircraftObject.transform.localRotation =
                            new Quaternion(newState.Qx, newState.Qy, newState.Qz, newState.Qw);
                    }
                    else
                    {
                        aircraftObjects.Add(
                            newState.AircraftID,
                            Instantiate(
                                aircraftPrefab,
                                new Vector3(newState.Px, newState.Py, newState.Pz),
                                new Quaternion(newState.Qx, newState.Qy, newState.Qz, newState.Qw),
                                transform
                            )
                        );
                    }
                }
            }
        }
    }
}