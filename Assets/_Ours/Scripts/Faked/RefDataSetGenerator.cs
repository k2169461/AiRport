using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;

namespace Vivraan.KU.AiRport.Faked
{
    public class RefDataSetGenerator : MonoBehaviour
    {
        public struct RefData
        {
            [UsedImplicitly]
            public int AircraftID;

            [UsedImplicitly]
            public float PxRef;

            [UsedImplicitly]
            public float PyRef;

            [UsedImplicitly]
            public float PzRef;

            [UsedImplicitly]
            public float QxRef;

            [UsedImplicitly]
            public float QyRef;

            [UsedImplicitly]
            public float QzRef;

            [UsedImplicitly]
            public float QwRef;

            [UsedImplicitly]
            public sealed class ClassMap : ClassMap<RefData>
            {
                public ClassMap()
                {
                    Map(m => m.AircraftID).Name("aircraft_id");
                    Map(m => m.PxRef).Name("px_ref");
                    Map(m => m.PyRef).Name("py_ref");
                    Map(m => m.PzRef).Name("pz_ref");
                    Map(m => m.QxRef).Name("qx_ref");
                    Map(m => m.QyRef).Name("qy_ref");
                    Map(m => m.QzRef).Name("qz_ref");
                    Map(m => m.QwRef).Name("qw_ref");
                }
            }
        }

        [ContextMenu(nameof(GenerateRefDataSet))]
        private void GenerateRefDataSet()
        {
            Assert.AreEqual("Waypoints", name, "This script must be attached to a GameObject named \"Waypoints\"");
            Assert.IsTrue(transform.childCount >= 2,
                "There must be at least two waypoints as children of the GameObject \"Waypoints\"");
            foreach (Transform child in transform)
            {
                Assert.AreEqual("RefDataWaypoint", child.tag,
                    "All children of the GameObject named \"Waypoints\" must have the tag \"RefDataWaypoint\"");
            }

            var id = transform.GetChild(0).name[0] - '0';
            using var writer = new StreamWriter($"Assets/_Ours/Faked/aircraft{id}_ref.csv");
            using var csv = new CsvWriter(writer, CultureInfo.InvariantCulture);
            csv.Context.RegisterClassMap<RefData.ClassMap>();

            csv.WriteHeader<RefData>();
            csv.NextRecord();
            foreach (Transform child in transform)
            {
                var position = child.position;
                var rotation = child.rotation;
                RefData waypoint = new()
                {
                    AircraftID = id,
                    PxRef = position.x,
                    PyRef = position.y,
                    PzRef = position.z,
                    QxRef = rotation.x,
                    QyRef = rotation.y,
                    QzRef = rotation.z,
                    QwRef = rotation.w
                };
                csv.WriteRecord(waypoint);
                csv.NextRecord();
            }
        }
    }
}
