using System;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;

namespace Vivraan.KU.AiRport.Faked
{
    public struct FakeFlightState
    {
        [UsedImplicitly]
        public int AircraftID;

        [UsedImplicitly]
        public int T;

        [UsedImplicitly]
        public float Px;

        [UsedImplicitly]
        public float Py;

        [UsedImplicitly]
        public float Pz;

        [UsedImplicitly]
        public float Qx;

        [UsedImplicitly]
        public float Qy;

        [UsedImplicitly]
        public float Qz;

        [UsedImplicitly]
        public float Qw;

        [UsedImplicitly]
        public sealed class ClassMap : ClassMap<FakeFlightState>
        {
            public ClassMap()
            {
                Map(m => m.AircraftID).Name("aircraft_id");
                Map(m => m.T).Name("t");
                Map(m => m.Px).Name("px");
                Map(m => m.Py).Name("py");
                Map(m => m.Pz).Name("pz");
                Map(m => m.Qx).Name("qx");
                Map(m => m.Qy).Name("qy");
                Map(m => m.Qz).Name("qz");
                Map(m => m.Qw).Name("qw");
            }
        }

        public override string ToString() =>
            $"aircraft_id: {AircraftID}, t: {T}, P(x: {Px}, y: {Py}, z: {Pz}), Q(x: {Qx}, y: {Qy}, z: {Qz}, w: {Qw})";
    }

    public struct FakeFlightResponse
    {
        public int Time;
        public FakeFlightState[] States;
    }

    public sealed class ClientWrapper : MonoBehaviour
    {
        [SerializeField, Min(0F)]
        private int apiRequestIntervalSeconds = 1;

        private float apiRequestCounterSeconds;
        public Action<FakeFlightResponse> OnFlightsUpdated;

        [FormerlySerializedAs("flightData")]
        [SerializeField]
        private TextAsset flightDataAsset;

        private FakeFlightState[][] flightsGroupedByTime;
        private int timeIndex;

        private void Start()
        {
            using var reader = new StringReader(flightDataAsset.text);
            using var csv = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true
            });
            csv.Context.RegisterClassMap<FakeFlightState.ClassMap>();
            flightsGroupedByTime = csv.GetRecords<FakeFlightState>()
                .GroupBy(flight => flight.T)
                .Select(group => group.ToArray())
                .ToArray();
        }

        private void FixedUpdate()
        {
            apiRequestCounterSeconds += Time.fixedDeltaTime;

            if (apiRequestCounterSeconds < apiRequestIntervalSeconds)
            {
                return;
            }

            apiRequestCounterSeconds = 0F;

            if (timeIndex >= flightsGroupedByTime.Length)
            {
                timeIndex = 0;
                print("Sending the aircraft back to their starting position!");
            }

            var flightStates = flightsGroupedByTime[timeIndex++];
            OnFlightsUpdated?.Invoke(new FakeFlightResponse { Time = flightStates[0].T, States = flightStates });
        }
    }
}