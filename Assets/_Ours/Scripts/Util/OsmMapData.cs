using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;

namespace Vivraan.KU.AiRport.Util
{
    [CreateAssetMenu(menuName = "AiRport/Create OsmMapData", fileName = "OsmMapData", order = 0)]
    public sealed class OsmMapData : ScriptableObject
    {
        [FormerlySerializedAs("Lat")]
        [Range(-90F, 90F)]
        public float lat;

        [FormerlySerializedAs("Lon")]
        [Range(-180F, 180F)]
        public float lon;

        [Range(-90F, 90F), Tooltip("WGS84")]
        public float minLat;

        [Range(-90F, 90F), Tooltip("WGS84")]
        public float maxLat;

        [Range(-180F, 180F), Tooltip("WGS84")]
        public float minLon;

        [Range(-180F, 180F), Tooltip("WGS84")]
        public float maxLon;

        [FormerlySerializedAs("mapWidth")]
        [Min(0F)]
        public int mapWidthMetres;

        [FormerlySerializedAs("mapHeight")]
        [Min(0F)]
        public int mapHeightMetres;

        private void OnValidate()
        {
            Assert.IsTrue(minLon < maxLon
                          && minLat < maxLat
                          && lat >= minLat && lat <= maxLat
                          && lon >= minLon && lon <= maxLon
                          && mapWidthMetres > 0 && mapHeightMetres > 0);
        }
    }
}