﻿using System;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;

namespace Vivraan.KU.AiRport.Editor
{
    [InitializeOnLoad]
    public class CompilationController
    {
        private static bool isAutoCompileEnabled;

        static CompilationController()
        {
            isAutoCompileEnabled = false;
            EditorApplication.LockReloadAssemblies();
            CompilationPipeline.compilationFinished += OnCompilationFinished;
        }

        [MenuItem("Tools/Toggle Auto Compile")]
        public static void ToggleAutoCompile()
        {
            isAutoCompileEnabled = !isAutoCompileEnabled;
            CompilationPipeline.compilationFinished += OnCompilationFinished;

            if (isAutoCompileEnabled)
            {
                EditorApplication.UnlockReloadAssemblies();
                CompilationPipeline.compilationFinished -= OnCompilationFinished;
            }
            else
            {
                EditorApplication.LockReloadAssemblies();
            }

            Debug.Log($"Auto Compile is {(isAutoCompileEnabled ? "Enabled" : "Disabled")}.");
        }

        [MenuItem("Tools/Compile Scripts")]
        public static void CompileScripts()
        {
            if (isAutoCompileEnabled)
            {
                Debug.Log("Auto Compile is Enabled. No action taken.");
                return;
            }

            EditorApplication.UnlockReloadAssemblies();
            AssetDatabase.Refresh();
        }

        private static void OnCompilationFinished(object obj)
        {
            EditorApplication.LockReloadAssemblies();
            Debug.Log("Scripts Compiled");
        }
    }
}