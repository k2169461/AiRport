using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;
using Vivraan.KU.AiRport.Util;

namespace Vivraan.KU.AiRport.OsnAPI
{
    public sealed class AircraftVizard : MonoBehaviour
    {
        [SerializeField]
        private OsmMapData mapData;

        [SerializeField]
        private GameObject aircraftPrefab;

        private ClientWrapper client;
        private Dictionary<string, GameObject> aircraftObjectsByIcao24;

        private void Start()
        {
            client = FindObjectOfType<ClientWrapper>();
            client.OnFlightsUpdated += InstantiateAircraftFirstTime;
            client.OnFlightsUpdated += UpdateAircraft;
            return;

            void InstantiateAircraftFirstTime(FlightStatesResponse response)
            {
                aircraftObjectsByIcao24 ??= new Dictionary<string, GameObject>(response.States.Length);
                foreach (var stateVector in response.States)
                {
                    if (stateVector.Latitude.HasValue && stateVector.Longitude.HasValue)
                    {
                        var (newPos, newRot) = NewPosRot(stateVector);
                        CreateAircraft(stateVector, newPos, newRot);
                    }
                }

                client.OnFlightsUpdated -= InstantiateAircraftFirstTime;
            }

            void UpdateAircraft(FlightStatesResponse response)
            {
                // Update and add the new
                foreach (var newStateVector in response.States)
                {
                    if (newStateVector.Latitude.HasValue && newStateVector.Longitude.HasValue)
                    {
                        var (newPos, newRot) = NewPosRot(newStateVector);
                        if (aircraftObjectsByIcao24.TryGetValue(newStateVector.Icao24, out var aircraftObject))
                        {
                            aircraftObject.transform.localPosition = newPos;
                            aircraftObject.transform.localRotation = newRot;
                        }
                        else
                        {
                            CreateAircraft(newStateVector, newPos, newRot);
                        }
                    }
                }

                // Delete the old
                var keys = aircraftObjectsByIcao24.Keys.OrderByDescending(x => x).ToArray();
                foreach (var key in keys)
                {
                    if (Array.FindIndex(response.States, x => x.Icao24 == key) == -1)
                    {
                        Destroy(aircraftObjectsByIcao24[key]);
                        aircraftObjectsByIcao24.Remove(key);
                    }
                }
            }

            (Vector3 pos, Quaternion rot) NewPosRot(in StateVector stateVector)
            {
                var newPos = new Vector3(
                    (float)math.remap(
                        mapData.minLon,
                        mapData.maxLon,
                        // Accounting for the fact that the map is centered at 0,0
                        -mapData.mapWidthMetres * 0.5D,
                        mapData.mapWidthMetres * 0.5D,
                        stateVector.Longitude!.Value),
                    (float)(stateVector.GeoAltitude ?? 0D),
                    (float)math.remap(mapData.minLat,
                        mapData.maxLat,
                        -mapData.mapHeightMetres * 0.5D,
                        mapData.mapHeightMetres * 0.5D,
                        stateVector.Latitude!.Value)
                );

                stateVector.VerticalRate ??= 0D;
                var newRot = Quaternion.Euler(
                    0F,
                    (float)(stateVector.TrueTrack ?? 0D),
                    stateVector.VerticalRate.Value > 0D ? 45F :
                    stateVector.VerticalRate.Value < 0D ? -45F : 0F);

                return (newPos, newRot);
            }

            void CreateAircraft(in StateVector stateVector, in Vector3 newPos, in Quaternion newRot)
            {
                var aircraftObject = Instantiate(aircraftPrefab, transform);
                aircraftObject.name = stateVector.Icao24;
                aircraftObject.transform.localPosition = newPos;
                aircraftObject.transform.localRotation = newRot;
                aircraftObjectsByIcao24.Add(stateVector.Icao24, aircraftObject);
            }
        }
    }
}