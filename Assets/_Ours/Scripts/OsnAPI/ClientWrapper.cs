using System;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Serialization;
using Vivraan.KU.AiRport.Util;

namespace Vivraan.KU.AiRport.OsnAPI
{
    // TODO Master Logic for OpenSkyNetwork API
    // Retrieve arrivals
    // Retrieve tracks of as many aircraft as possible
    // Feed back data as a dictionary?

    [JsonConverter(typeof(StateVectorConverter))]
    public sealed class StateVector
    {
        public string Icao24;
        public long? TimePosition;
        public double? Longitude;
        public double? Latitude;
        public double? BaroAltitude;
        public double? TrueTrack;
        public double? VerticalRate;
        public double? GeoAltitude;
        public double? Velocity;
        public int Category;
        public bool OnGround;
    }

    public sealed class FlightStatesResponse
    {
        [JsonProperty("time", Required = Required.Always)]
        public long Time;

        [JsonConverter(typeof(StateVectorArrayConverter)), JsonProperty("states", Required = Required.Always)]
        public StateVector[] States;
    }

    // https://github.dev/steveberdy/OpenSky/
    public sealed class StateVectorConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(StateVector);

        public override bool CanRead => true;

        public override bool CanWrite => false;

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var jsonArray = JArray.Load(reader);

            return new StateVector
            {
                Icao24 = jsonArray[0].Value<string>(),
                // Callsign = jsonArray[1].Value<string>()?.Trim(),
                // OriginCountry = jsonArray[2].Value<string>(),
                // TimePosition = jsonArray[3].Value<int?>().FromUnixTimestamp(),
                // LastContact = jsonArray[4].Value<int>(),
                Longitude = jsonArray[5].Value<float?>(),
                Latitude = jsonArray[6].Value<float?>(),
                BaroAltitude = jsonArray[7].Value<float?>(),
                OnGround = jsonArray[8].Value<bool>(),
                // Velocity = jsonArray[9].Value<float?>(),
                TrueTrack = jsonArray[10].Value<float?>(),
                VerticalRate = jsonArray[11].Value<float?>(),
                // Sensors = jsonArray[12].Value<int[]>(),
                GeoAltitude = jsonArray[13].Value<float?>(),
                // Squawk = jsonArray[14].Value<string>(),
                // Spi = jsonArray[15].Value<bool>(),
                // PositionSource = jsonArray[16].Value<int>()
            };
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            => throw new NotImplementedException();
    }

    // https://github.dev/steveberdy/OpenSky/
    public sealed class StateVectorArrayConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(StateVector[]);

        public override bool CanRead => true;

        public override bool CanWrite => false;

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            return reader.TokenType == JsonToken.StartArray ? serializer.Deserialize<StateVector[]>(reader) : null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            => throw new NotImplementedException();
    }

    public class ClientWrapper : MonoBehaviour
    {
        private const string BaseUri = "https://opensky-network.org/api";
        private const string IcaoShanghaiHongqiao = "ZSSS";
        private const string IcaoAlaskaAnchorage = "PANC";

        [SerializeField]
        private OsmMapData mapData;

        [SerializeField]
        private Secrets secrets;

        [SerializeField, Min(0F)]
        private int apiRequestIntervalSeconds = 1;

        [FormerlySerializedAs("airportArrivalsTrackingDelaySeconds")]
        [SerializeField, Min(0F)]
        private int flightTrackingDelaySeconds = 10;

        private float apiRequestCounterSeconds;
        private long prevAPICallStartUnixSeconds;
        public Action<FlightStatesResponse> OnFlightsUpdated;

        private FlightStatesResponse statesResponse;

        private void Start()
        {
            prevAPICallStartUnixSeconds = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        }

        private void FixedUpdate()
        {
            apiRequestCounterSeconds += Time.fixedDeltaTime;

            if (apiRequestCounterSeconds < apiRequestIntervalSeconds)
            {
                return;
            }

            apiRequestCounterSeconds = 0F;

            var apiCallStartUnixSeconds = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            var timeDiff = apiCallStartUnixSeconds - prevAPICallStartUnixSeconds;

            if (statesResponse == null || timeDiff >= flightTrackingDelaySeconds)
            {
                StartCoroutine(GetStateVectors(apiCallStartUnixSeconds));
            }
        }

        private IEnumerator GetStateVectors(long apiCallStartTime)
        {
            var request = UnityWebRequest.Get(
                $"{BaseUri}/states/all"
                + $"?lamin={mapData.minLat}"
                + $"&lomin={mapData.minLon}"
                + $"&lamax={mapData.maxLat}"
                + $"&lomax={mapData.maxLon}");
            // request.SetRequestHeader("Authorization", $"Basic {secrets.username}:{secrets.password}");

            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                yield break;
            }

            var nativeData = request.downloadHandler.nativeData;
            var response = string.Create(nativeData.Length, nativeData, (span, data) =>
            {
                for (var i = 0; i < data.Length; i++)
                {
                    span[i] = (char)data[i];
                }
            });
            statesResponse = JsonConvert.DeserializeObject<FlightStatesResponse>(response);
            prevAPICallStartUnixSeconds = apiCallStartTime;
            OnFlightsUpdated?.Invoke(statesResponse);
        }
    }
}