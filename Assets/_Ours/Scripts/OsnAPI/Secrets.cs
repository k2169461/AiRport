using UnityEngine;
using UnityEngine.Assertions;

namespace Vivraan.KU.AiRport.OsnAPI
{
    [CreateAssetMenu(menuName = "AiRport/Create OpenSkyNetwork API Secrets", fileName = "OsnAPISecrets", order = 0)]
    public class Secrets : ScriptableObject
    {
        public string username;
        public string password;

        private void OnValidate()
        {
            Assert.IsFalse(string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password),
                "OpenSkyNetwork API Secrets are not set!");
        }
    }
}